﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShapeData
{
    public string name;
    public Vector3[] blocksPositions;
    public int appearanceOdds;
}
